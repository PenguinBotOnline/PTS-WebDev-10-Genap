import React from 'react';
import ListData from "./ListData.js";
import data from "../utils/data.js";
import DataInput from './DataInput.js';

class ContainerData extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dataList: data
        };

        this.onDelete = this.onDelete.bind(this);
        this.onAddAnimal = this.onAddAnimal.bind(this);
    }

    render() {
        return (
            <div className="container-data">
                <h1>Aplikasi Data Hewan</h1>
                <h2>Tambah hewan</h2>
                <DataInput addAnimal={this.onAddAnimal} />
                <h2>List data hewan</h2>
                <ListData animalData={this.state.dataList} onDelete={this.onDelete} />
            </div>
        );
    }

    onDelete(id) {
        const newDataList = this.state.dataList.filter(animal => !(animal.id === id));
        this.setState({ dataList: newDataList });
    }

    onAddAnimal({ name, description }) {
        this.setState((prevState) => {
            return {
                dataList: [
                    ...prevState.dataList,
                    {
                        id: +new Date(),
                        name,
                        description,
                        img: '/img/default.png'
                    }
                ]
            }
        });
    }
}

export default ContainerData;