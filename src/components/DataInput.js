import React from "react";

class DataInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            description: "",
            id: ""
        };

        this.onNameChange = this.onNameChange.bind(this);
        this.onDescriptionChange = this.onDescriptionChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onNameChange(event) {
        this.setState(() => {
            return { name: event.target.value, }
        });
    }

    onDescriptionChange(event) {
        this.setState(() => {
            return { description: event.target.value, }
        })
    }

    onSubmit(event) {
        event.preventDefault();
        this.props.addAnimal(this.state);
        this.setState(() => {
            return {
                name: "",
                description: ""
            }
        });
    }

    render() {
        return (
            <form className="data-input" onSubmit={this.onSubmit}>
                <label>
                    Nama hewan:
                    <br></br>
                    <input
                        type="text"
                        placeholder="Animal name"
                        className="name-input"
                        name={this.state.name}
                        value={this.state.name}
                        onChange={this.onNameChange}>
                    </input>
                </label>
                <label>
                    Deskripsi hewan:
                    <br></br>
                    <textarea
                        placeholder="Animal description"
                        className="desc-input"
                        description={this.state.description}
                        value={this.state.description}
                        onChange={this.onDescriptionChange}>
                    </textarea>
                </label>
                <button
                    type="submit"
                    className="submit-button">
                    Submit
                </button>
            </form>
        );
    }
}

export default DataInput;