import ItemData from "./ItemData.js";

function ListData({ animalData, onDelete }) {
    return (
        <ul className="list-data">
            {
                animalData.map((animal) => {
                    return (<ItemData animal={animal} key={animal.name + "-key"} id={animal.id} onDelete={onDelete} />);
                })
            }
        </ul>
    );
}

export default ListData;