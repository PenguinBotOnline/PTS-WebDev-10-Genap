import ItemImage from "./ItemImage.js";
import ItemBody from "./ItemBody.js";
import DeleteButton from "./DeleteButton.js";

function ItemData({ animal, id, onDelete }) {
    return (
        <li key={animal.name + '-key'} className="item-data">
            <ItemImage img={animal.img} name={animal.name} />
            <ItemBody name={animal.name} desc={animal.description} />
            <DeleteButton id={id} onDelete={onDelete} />
        </li>
    );
}

export default ItemData;