import React from 'react';
import ReactDOM from 'react-dom/client';
import './styles/index.css';
import ContainerData from './components/ContainerData.js';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <ContainerData />
);