const animalData = [
    {
        name: 'Kucing',
        description: 'Kucing disebut juga kucing domestik atau kucing rumah (nama ilmiah: Felis silvestris catus atau Felis catus) adalah sejenis mamalia karnivora dari keluarga Felidae. Kata "kucing" biasanya merujuk kepada "kucing" yang telah dijinakkan, tetapi bisa juga bisa merujuk kepada "kucing besar" seperti singa dan harimau yang juga termasuk jenis kucing.',
        img: 'img/kucing.jpg',
        id: 1
    },
    {
        name: 'Anjing',
        description: 'Anjing domestik atau Anjing (Canis lupus familiaris) adalah hewan mamalia yang telah mengalami domestikasi dari serigala sejak 15.000 tahun yang lalu, bahkan kemungkinan sudah sejak 100.000 tahun yang lalu berdasarkan bukti genetik berupa penemuan fosil dan tes DNA. Penelitian lain mengungkap sejarah domestikasi anjing yang belum begitu lama.',
        img: 'img/anjing.jpg',
        id: 2
    },
    {
        name: 'Masbro',
        description: 'Kapibara atau kapibara besar (Hydrochoerus hydrochaeris) merupakan jenis hewan pengerat terbesar yang masih ada di dunia (hewan pengerat terbesar yang telah punah adalah Phoberomys pattersoni) yang merupakan anggota genus Hydrochoerus. Kapibara merupakan hewan asli daerah tropis dan lembap di Amerika Selatan. Kapibara dapat ditemukan di daerah Timur Andes dari wilayah Kanal Panama sampai daerah utara Kolombia dan Venezuela, Uruguay, dan Provinsi Buenos Aires di Argentina. Kapibara mendiami sabana dan hutan lebat, dan hidup di dekat perairan. Spesies ini sangat sosial dan dapat ditemukan berkelompok terdiri dari 100 individu, namun biasanya hidup berkelompok terdiri dari 10–20 individu. Kapibara diburu untuk diambil daging dan kulitnya, dan juga minyak dari kulitnya yang tebal berlemak. Kapibara tidak dianggap sebagai spesies terancam.',
        img: 'img/masbro.jpg',
        id: 3
    },
    {
        name: 'Kerbau',
        description: 'Kerbau, atau biasa disebut kerbau air (untuk membedakannya dengan kerbau afrika), adalah binatang memamah biak yang menjadi ternak bagi banyak bangsa di dunia, terutama Asia dan Amerika Selatan. Hewan ini adalah domestikasi dari kerbau liar (orang India menyebutnya arni) yang masih dapat ditemukan di daerah-daerah Pakistan, India, Bangladesh, Nepal, Bhutan, Vietnam, China, Filipina, Taiwan, Indonesia, dan Thailand, Malaysia.',
        img: 'img/kerbau.jpg',
        id: 4
    },
    {
        name: 'Ayam',
        description: 'Ayam atau kotok (SD) (Gallus gallus domesticus) adalah binatang unggas yang biasa dipelihara untuk dimanfaatkan daging, telur, dan bulunya. Ayam peliharaan merupakan keturunan langsung dari salah satu subspesies ayam hutan yang dikenal sebagai ayam hutan merah (Gallus gallus) atau ayam bangkiwa (bankiva fowl). Semua ayam (dan juga semua jenis burung lainnya) merupakan bagian dari dinosaurus theropoda seperti Tyrannosaurus rex. Kawin silang antar ras ayam telah menghasilkan ratusan galur unggul atau galur murni dengan bermacam-macam fungsi; yang paling umum adalah ayam potong (untuk dipotong) dan ayam petelur (untuk diambil telurnya). Ayam biasa dapat pula dikawin silang dengan kerabat dekatnya, ayam hutan hijau, yang menghasilkan hibrida mandul yang jantannya dikenal sebagai ayam bekisar.',
        img: 'img/ayam.jpg',
        id: 5
    },
    {
        name: 'Axolotl',
        description: 'Axolotl berasal dari bahasa Nāhuatl āxōlōtl ("monster air") atau salamander meksiko (Ambystoma mexicanum) adalah salamander neotenik yang berhubungan dekat dengan salamander harimau. Larva spesies ini tidak mengalami metamorfosis, sehingga axolotl dewasa tetap bersifat akuatik dan memiliki insang. Spesies ini juga disebut ajolote (juga merupakan nama umum untuk jenis-jenis salamander yang berbeda). Axolotl berasal dari berbagai danau, seperti Danau Xochimilco dan hewan ini menjadi hewan endemik Meksiko. Hewan ini sering digunakan dalam penelitian ilmiah karena mereka mampu meregenerasi hampir seluruh anggota tubuh. Kemampuan ini membuat sebagian orang menganggap axolotl sebagai hewan yang luar biasa.',
        img: 'img/axolotl.jpg',
        id: 6
    }
]

export default animalData;