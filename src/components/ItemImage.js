function ItemImage({ img, name }) {
    return (
        <img src={img} alt={name + "-alt"} className="item-image" />
    );
}

export default ItemImage;