function ItemBody({ name, desc }) {

    return (
        <div className='item-body'>
            <h2>{name}</h2>
            <p>{desc}</p>
        </div>
    );
}

export default ItemBody;